let colors = ["pallet", "viridian", "pewter", "cerulean", "vermillion", "lavender", "caladon", "saffon", "fuschia", "cinnabar"];

let renderButton = () => {
    let contentHTML = ""
    for (var index = 0; index < colors.length; index++) {
        let color = colors[index];
        contentHTML += `<button class="btn color-button ${color}" style="background-color:${color};border:3px solid gray;margin:0 10px; " onclick="changeColor('${color}')"></button>`
    }
    document.getElementById("colorContainer").innerHTML = contentHTML;

}
renderButton();
let changeColor = (color) => {
    var chooseColor = color;
    for (var index = 0; index < colors.length; index++) {
        if (chooseColor != color[index]) {
            document.querySelector(".house").classList.remove(colors[index]);
        }
    }
    document.querySelector(".house").classList.add(`${color}`)


}
