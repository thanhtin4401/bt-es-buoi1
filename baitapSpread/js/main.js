
// spread Operator
let hoverMe = "Hover Me!";
let chart = [...hoverMe];
var HTML = '';
for (let character of chart) {
    let contentSpan = `<span>${character}</span>`;
    HTML += contentSpan;
}
document.querySelector('.heading').innerHTML = HTML;
